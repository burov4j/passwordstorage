﻿using System.Windows;
using System.Windows.Input;

namespace PasswordStorage
{
    public partial class GroupWindow : Window
    {
        public GroupWindow()
        {
            InitializeComponent();
            resultTextBox.Focus();
        }

        public string ResultText
        {
            get { return resultTextBox.Text; }
            set { resultTextBox.Text = value; }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    DialogResult = true;
                    break;
                case Key.Escape:
                    DialogResult = false;
                    break;
            }
        }
    }
}
