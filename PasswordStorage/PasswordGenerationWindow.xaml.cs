﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace PasswordStorage
{
    public partial class PasswordGenerationWindow : Window
    {
        public PasswordGenerationWindow()
        {
            InitializeComponent();
            GeneratePassword();
        }

        public string Text 
        {
            get { return passwordTextBox.Text; }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    DialogResult = true;
                    break;
                case Key.Escape:
                    DialogResult = false;
                    break;
            }
        }

        private void Generation_Click(object sender, RoutedEventArgs e)
        {
            GeneratePassword();
        }

        private void GeneratePassword()
        {
            if (passwordLength.Value.HasValue && (cb1.IsChecked.Value || cb2.IsChecked.Value || cb3.IsChecked.Value || cb4.IsChecked.Value))
            {
                char[] array1 = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' },
                       array2 = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' },
                       array3 = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' },
                       array4 = new char[] { '@', '#', '!' },
                       commonArray = new char[0];
                if (cb1.IsChecked.Value) commonArray = commonArray.Concat(array1).ToArray();
                if (cb2.IsChecked.Value) commonArray = commonArray.Concat(array2).ToArray();
                if (cb3.IsChecked.Value) commonArray = commonArray.Concat(array3).ToArray();
                if (cb4.IsChecked.Value) commonArray = commonArray.Concat(array4).ToArray();
                Random rand = new Random();
                StringBuilder result = new StringBuilder();
                for (int i = 0; i < passwordLength.Value.Value; i++) result.Append(commonArray[rand.Next(commonArray.Length)]);
                passwordTextBox.Text = result.ToString();
            }
        }
    }
}
