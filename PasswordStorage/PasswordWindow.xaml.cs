﻿using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace PasswordStorage
{
    public partial class PasswordWindow : Window
    {
        public PasswordWindow(ObservableCollection<Groups> groupsCollection)
        {
            InitializeComponent();
            groupsComboBox.ItemsSource = groupsCollection;
            titleTextBox.Focus();
        }

        public string PasswordTitle
        {
            get { return titleTextBox.Text; }
            set { titleTextBox.Text = value; }
        }

        public string Login
        {
            get { return loginTextBox.Text; }
            set { loginTextBox.Text = value; }
        }

        public string Password
        {
            get { return passwordTextBox.Text; }
            set { passwordTextBox.Text = value; }
        }

        public string URL
        {
            get { return URLTextBox.Text; }
            set { URLTextBox.Text = value; }
        }

        public string Notes
        {
            get { return notesTextBox.Text; }
            set { notesTextBox.Text = value; }
        }

        public Groups Group
        {
            get { return groupsComboBox.SelectedValue as Groups; }
            set { groupsComboBox.SelectedValue = value; }
        }

        private void PasswordVisibility_Click(object sender, RoutedEventArgs e)
        {
            passwordTextBox.IsPasswordVisible = !passwordTextBox.IsPasswordVisible;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    DialogResult = true;
                    break;
                case Key.Escape:
                    DialogResult = false;
                    break;
            }
        }

        private void PasswordGeneration_Click(object sender, RoutedEventArgs e)
        {
            PasswordGenerationWindow passwordWindow = new PasswordGenerationWindow() { Owner = this };
            if (passwordWindow.ShowDialog().Value)
            {
                passwordTextBox.Text = passwordWindow.Text;
                Properties.Settings.Default.Save();
            }
        }
    }
}
