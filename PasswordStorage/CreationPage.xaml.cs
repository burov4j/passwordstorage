﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using System.IO;

namespace PasswordStorage
{
    public partial class CreationPage : Page
    {
        PasswordStorageDatabaseEntities databaseEntities;
        NavigationWindow creationWindow;

        public CreationPage(PasswordStorageDatabaseEntities databaseEntities)
        {
            InitializeComponent();
            this.databaseEntities = databaseEntities;
            databaseNameBox.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Password Storage Database.sdf";
            passwordBox1.Focus();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            creationWindow.DialogResult = false;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            Create();
        }

        private void Create()
        {
            if (passwordBox1.Password == String.Empty)
            {
                MessageBox.Show(Properties.Resources.EnterThePasswordToProtect, Properties.Resources.Warning, MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (passwordBox1.Password != passwordBox2.Password)
            {
                MessageBox.Show(Properties.Resources.PasswordsDoNotMatch, Properties.Resources.Warning, MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            databaseEntities.Database.Connection.Close();
            databaseEntities.Database.Connection.ConnectionString = String.Format(@"data source={0};password={1}", databaseNameBox.Text, passwordBox1.Password);
            try
            {
                if (File.Exists(databaseNameBox.Text)) File.Delete(databaseNameBox.Text);
                databaseEntities.Database.Create();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Properties.Resources.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            PasswordStorage.Properties.Settings.Default.Save();
            creationWindow.DialogResult = true;
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog()
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Filter = Properties.Resources.DatabaseFile + " (*.sdf)|*.sdf",
                Title = Properties.Resources.CreateADatabase
            };
            if (saveDialog.ShowDialog().Value)
            {
                databaseNameBox.Text = saveDialog.FileName;
                passwordBox1.Focus();
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            creationWindow = Parent as NavigationWindow;
        }

        private void Page_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Create();
        }
    }
}
