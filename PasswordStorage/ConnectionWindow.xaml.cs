﻿using System.Windows.Input;
using System.Windows.Navigation;

namespace PasswordStorage
{
    public enum ConnectionWindowMode { Creation, Connection }

    public partial class ConnectionWindow : NavigationWindow
    {
        public ConnectionWindow(PasswordStorageDatabaseEntities databaseEntities, ConnectionWindowMode Mode)
        {
            InitializeComponent();
            if (Mode == ConnectionWindowMode.Connection) Navigate(new ConnectionPage(databaseEntities));
            else Navigate(new CreationPage(databaseEntities));
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape) DialogResult = false;
        }
    }
}
