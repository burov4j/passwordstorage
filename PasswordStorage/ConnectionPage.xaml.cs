﻿using Microsoft.Win32;
using System;
using System.Data.SqlServerCe;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;

namespace PasswordStorage
{
    public partial class ConnectionPage : Page
    {
        PasswordStorageDatabaseEntities databaseEntities;
        NavigationWindow connectionWindow;

        public ConnectionPage(PasswordStorageDatabaseEntities databaseEntities)
        {
            InitializeComponent();
            this.databaseEntities = databaseEntities;
            passwordBox.Focus();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
           connectionWindow.DialogResult = false;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            Connect();
        }

        private void Connect()
        {
            databaseEntities.Database.Connection.Close();
            databaseEntities.Database.Connection.ConnectionString = String.Format(@"data source={0};password={1}", databaseNameBox.Text, passwordBox.Password);
            try
            {
                databaseEntities.Database.Connection.Open();
            }
            catch (SqlCeException ex)
            {
                switch (ex.NativeError)
                {
                    case 25028:
                        MessageBox.Show(Properties.Resources.IncorrectPassword, Properties.Resources.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    case 25046:
                        MessageBox.Show(Properties.Resources.DatabaseFileIsNotFound, Properties.Resources.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    default:
                        MessageBox.Show(ex.Message, Properties.Resources.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Properties.Resources.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Properties.Settings.Default.Save();
            connectionWindow.DialogResult = true;
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog()
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Filter = Properties.Resources.DatabaseFile + " (*.sdf)|*.sdf",
                Title = Properties.Resources.OpenADatabase
            };
            if (openDialog.ShowDialog().Value)
            {
                databaseNameBox.Text = openDialog.FileName;
                passwordBox.Focus();
            }
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new CreationPage(databaseEntities));
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            connectionWindow = Parent as NavigationWindow;
        }

        private void Page_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Connect();
        }
    }
}
