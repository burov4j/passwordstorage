﻿using System.Windows;
using System.Windows.Controls;
using System;
using System.ComponentModel;

namespace PasswordStorage
{
    public partial class PasswordTextBox : UserControl, INotifyPropertyChanged
    {
        public PasswordTextBox()
        {
            InitializeComponent();
        }

        public bool IsPasswordVisible
        {
            get { return !hiddenTextBox.IsVisible; }
            set 
            {
                if (value)
                {
                    hiddenTextBox.Visibility = Visibility.Hidden;
                    visibleTextBox.Visibility = Visibility.Visible;
                }
                else
                {
                    hiddenTextBox.Visibility = Visibility.Visible;
                    visibleTextBox.Visibility = Visibility.Hidden;
                }
            }
        }

        public bool IsReadOnly
        {
            get { return hiddenRect.IsVisible; }
            set 
            {
                if (value) hiddenRect.Visibility = Visibility.Visible;
                else hiddenRect.Visibility = Visibility.Hidden;
            }
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        private void TextBox_Changed(object sender, TextChangedEventArgs e)
        {
            Text = visibleTextBox.Text;
        }

        private void PasswordBox_Changes(object sender, RoutedEventArgs e)
        {
            Text = hiddenTextBox.Password;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        } 

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(PasswordTextBox), new PropertyMetadata(String.Empty, OnTextPropertyChanged));

        private static void OnTextPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            PasswordTextBox myUserControl = dependencyObject as PasswordTextBox;
            myUserControl.OnPropertyChanged("Text");
            myUserControl.OnTextPropertyChanged(e);
        }

        private void OnTextPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            visibleTextBox.Text = Text;
            if (hiddenTextBox.Password != Text) hiddenTextBox.Password = Text;
        }
    }
}
