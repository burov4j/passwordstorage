﻿using System;
using System.Windows;
using System.Windows.Input;

namespace PasswordStorage
{
    public partial class ChangePasswordWindow : Window
    {
        public ChangePasswordWindow()
        {
            InitializeComponent();
            passwordBox1.Focus();
        }

        public string Password
        {
            get { return passwordBox1.Password; }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            Change();
        }

        private void Change()
        {
            if (passwordBox1.Password == String.Empty)
            {
                MessageBox.Show(Properties.Resources.EnterThePasswordToProtect, Properties.Resources.Warning, MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (passwordBox1.Password != passwordBox2.Password)
            {
                MessageBox.Show(Properties.Resources.PasswordsDoNotMatch, Properties.Resources.Warning, MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    Change();
                    break;
                case Key.Escape:
                    DialogResult = false;
                    break;
            }
        }
    }
}
