﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlServerCe;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace PasswordStorage
{
    public partial class MainWindow : Window
    {
        PasswordStorageDatabaseEntities databaseEntities;

        public MainWindow()
        {
            if (Properties.Settings.Default.IsUpdateCheck)
            {
                const string fileName = "Password Storage Updater.exe";
                if (File.Exists(fileName)) Process.Start(fileName);
            }
            InitializeComponent();
            ConnectionWindow connectionWindow = new ConnectionWindow(databaseEntities = FindResource("PasswordStorageDatabase") as PasswordStorageDatabaseEntities, ConnectionWindowMode.Connection);
            if (!connectionWindow.ShowDialog().Value)
            {
                Close();
                return;
            }
            rootGroup.Header = System.IO.Path.GetFileNameWithoutExtension(databaseEntities.Database.Connection.DataSource);
            rootGroup.IsSelected = true;
            LoadData();
        }

        private void AddGroup_Click(object sender, RoutedEventArgs e)
        {
            GroupWindow addWindow = new GroupWindow() { Owner = this };
            if (addWindow.ShowDialog().Value)
            {
                databaseEntities.Groups.Add(new Groups() { Title = addWindow.ResultText });
                databaseEntities.SaveChanges();
            }
        }

        private void EditGroup_Click(object sender, RoutedEventArgs e)
        {
            Groups selectedGroup = treeViewGroup.SelectedValue as Groups;
            if (selectedGroup != null)
            {
                GroupWindow editWindow = new GroupWindow() { Title = Properties.Resources.ChangeTheDirectoryName, ResultText = selectedGroup.Title, Owner = this };
                if (editWindow.ShowDialog().Value)
                {
                    selectedGroup.Title = editWindow.ResultText;
                    databaseEntities.SaveChanges();
                }
            }
            else MessageBox.Show(Properties.Resources.SelectADirectoryToEdit, Properties.Resources.Warning, MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void TreeViewItem_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            (sender as TreeViewItem).IsSelected = true;
        }

        private void RemoveGroup_Click(object sender, RoutedEventArgs e)
        {
            Groups selectedGroup = treeViewGroup.SelectedValue as Groups;
            if (selectedGroup != null)
            {
                if (MessageBox.Show(Properties.Resources.DoYouWantToRemoveDirectory, Properties.Resources.Warning, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    databaseEntities.Groups.Remove(selectedGroup);
                    databaseEntities.SaveChanges();
                }
            }
            else MessageBox.Show(Properties.Resources.SelectADirectoryToRemove, Properties.Resources.Warning, MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void TreeView_SelectedChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Groups group = treeViewGroup.SelectedValue as Groups;
            if (group == null) passwordsDataGrid.ItemsSource = databaseEntities.Passwords.Local;
            else passwordsDataGrid.ItemsSource = group.Passwords;
        }

        private void AddPassword_Click(object sender, RoutedEventArgs e)
        {
            PasswordWindow addPassword = new PasswordWindow(databaseEntities.Groups.Local) { Owner = this, Group = treeViewGroup.SelectedValue as Groups };
            if (addPassword.ShowDialog().Value)
            {
                databaseEntities.Passwords.Add(new Passwords()
                {
                    Title = addPassword.PasswordTitle,
                    Login = addPassword.Login,
                    Password = addPassword.Password,
                    URL = addPassword.URL,
                    Notes = addPassword.Notes,
                    Groups = addPassword.Group
                });
                databaseEntities.SaveChanges();
            };
        }

        private void RemovePassword_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show(Properties.Resources.DoYouWantToRemovePasswords, Properties.Resources.Warning, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                List<Passwords> removedList = passwordsDataGrid.SelectedItems.Cast<Passwords>().ToList();
                foreach (Passwords entry in removedList) databaseEntities.Passwords.Remove(entry);
                databaseEntities.SaveChanges();
            }
        }

        private void EditPassword_Click(object sender, RoutedEventArgs e)
        {
            EditPassword(passwordsDataGrid.SelectedValue as Passwords);
        }

        private void EditPassword(Passwords editingPassword)
        {
            if (editingPassword != null)
            {
                PasswordWindow editPasswordWindow = new PasswordWindow(databaseEntities.Groups.Local)
                {
                    Title = Properties.Resources.EditThePassword,
                    Owner = this,
                    Group = editingPassword.Groups,
                    PasswordTitle = editingPassword.Title,
                    Login = editingPassword.Login,
                    Password = editingPassword.Password,
                    URL = editingPassword.URL,
                    Notes = editingPassword.Notes
                };
                if (editPasswordWindow.ShowDialog().Value)
                {
                    editingPassword.Groups = editPasswordWindow.Group;
                    editingPassword.Title = editPasswordWindow.PasswordTitle;
                    editingPassword.Login = editPasswordWindow.Login;
                    editingPassword.Password = editPasswordWindow.Password;
                    editingPassword.URL = editPasswordWindow.URL;
                    editingPassword.Notes = editPasswordWindow.Notes;
                    databaseEntities.SaveChanges();
                };
            }
            else MessageBox.Show(Properties.Resources.SelectAPasswordToEdit, Properties.Resources.Warning, MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void PasswordDataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditPassword((sender as DataGridRow).Item as Passwords);
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void CopyLogin_Click(object sender, RoutedEventArgs e)
        {
            Passwords selectedPassword = passwordsDataGrid.SelectedValue as Passwords;
            if (selectedPassword != null) Clipboard.SetText(selectedPassword.Login);
        }

        private void CopyPassword_Click(object sender, RoutedEventArgs e)
        {
            Passwords selectedPassword = passwordsDataGrid.SelectedValue as Passwords;
            if (selectedPassword != null) Clipboard.SetText(selectedPassword.Password);
        }

        private void CreateDatabase_Click(object sender, RoutedEventArgs e)
        {
            ConnectionWindow connectionWindow = new ConnectionWindow(databaseEntities, ConnectionWindowMode.Creation) { WindowStartupLocation = WindowStartupLocation.CenterOwner, Owner = this };
            if (connectionWindow.ShowDialog().Value) Refresh();
        }

        private void OpenDatabase_Click(object sender, RoutedEventArgs e)
        {
            ConnectionWindow connectionWindow = new ConnectionWindow(databaseEntities, ConnectionWindowMode.Connection) { WindowStartupLocation = WindowStartupLocation.CenterOwner, Owner = this };
            if (connectionWindow.ShowDialog().Value) Refresh();
        }

        private void Refresh()
        {
            foreach (var entry in databaseEntities.ChangeTracker.Entries()) entry.State = EntityState.Detached;
            LoadData();
        }

        private void LoadData()
        {
            databaseEntities.Groups.Load();
            databaseEntities.Passwords.Load();
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink link = (Hyperlink)(e.OriginalSource);
            string URL = link.NavigateUri.OriginalString;
            if (URL.StartsWith("http://") || URL.StartsWith("https://")) { Process.Start(URL); return; }
            if (URL.IndexOf("@") > -1) { Process.Start("mailto:" + URL); return; }
            Process.Start("http://" + link.NavigateUri.OriginalString);
        }

        private void ChangePassword_Click(object sender, RoutedEventArgs e)
        {
            ChangePasswordWindow passwordWindow = new ChangePasswordWindow() { Owner = this };
            if (passwordWindow.ShowDialog().Value)
            {
                SqlCeEngine engine = new SqlCeEngine(databaseEntities.Database.Connection.ConnectionString);
                databaseEntities.Database.Connection.Close();
                engine.Compact(databaseEntities.Database.Connection.ConnectionString = String.Format(@"data source={0};password={1}", databaseEntities.Database.Connection.DataSource, passwordWindow.Password));
            }
        }

        private void TextBlock_MouseMove(object sender, MouseEventArgs e)
        {
            TextBlock element = sender as TextBlock;
            if (element != null && e.LeftButton == MouseButtonState.Pressed)
            {
                DragDrop.DoDragDrop(element, element.Text, DragDropEffects.Copy);
            }
        }

        private void PasswordBox_MouseMove(object sender, MouseEventArgs e)
        {
            TextBlock element = (TextBlock)sender;
            if (element != null && e.LeftButton == MouseButtonState.Pressed)
            {
                DragDrop.DoDragDrop(element, element.Tag, DragDropEffects.Copy);
            } 
        }
    }
}
